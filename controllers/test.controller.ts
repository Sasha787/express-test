import { RequestHandler } from 'express'
import { TestReponse } from '../models/test.model'

export const test: RequestHandler = async (req, res) => {
  console.info(req.body)

  res.status(200).json(TestReponse)
}
